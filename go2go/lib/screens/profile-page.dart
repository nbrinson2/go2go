import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../screens/login.dart';

class Auth extends StatefulWidget {
  final bool isLoggedIn;
  const Auth({Key key, this.isLoggedIn}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _AuthState();
  }
}

class _AuthState extends State<Auth> {
  bool isLoggedIn;
  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);

  _logout() async {
    _googleSignIn.signOut();
    setState(() {
      isLoggedIn = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        
        children: <Widget>[
          Text(
            '${widget.isLoggedIn.toString()}',
            style: TextStyle(fontSize: 50),
          ),
          OutlineButton(
            child: Text("Logout"),
            onPressed: () {
              _logout();
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => LoginPage(isLoggedIn: isLoggedIn)),
              );
            },
          )
        ],
      )),
    ));
  }
}
