import 'package:flutter/material.dart';
import '../common/app_card.dart';
import '../screens/number-confirmation.dart';

class UserForm extends StatelessWidget {
  const UserForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController userName = new TextEditingController();
    TextEditingController email = new TextEditingController();
    TextEditingController password = new TextEditingController();
    TextEditingController phoneNumber = new TextEditingController();

    return Scaffold(
        body: Container(
            margin: EdgeInsets.only(top: 5.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Please enter the following to create an account',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15.0, color: Colors.green[700]),
                ),
                AppCard(
                  child: Container(
                    margin: EdgeInsets.only(top: 12.0),
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          decoration: InputDecoration(labelText: 'User Name'),
                          controller: userName,
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Email'),
                          controller: email,
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Password'),
                          controller: password,
                        ),
                        TextFormField(
                          decoration:
                              InputDecoration(labelText: 'Phone Number'),
                          controller: phoneNumber,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 5.0),
                          width: 150,
                          child: FlatButton(
                            color: Colors.green,
                            textColor: Colors.white,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => NumberConfirmation(
                                          userName: userName.text,
                                          email: email.text,
                                          phoneNumber: phoneNumber.text,
                                        )),
                              );
                            },
                            child: Text(
                              'Submit',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )));
  }
}
