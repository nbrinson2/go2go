import 'package:flutter/material.dart';
import '../common/app_card.dart';

class PasswordRecovery extends StatefulWidget {
  PasswordRecovery({Key key}) : super(key: key);

  @override
  _PasswordRecoveryState createState() => _PasswordRecoveryState();
}

class _PasswordRecoveryState extends State<PasswordRecovery> {
  TextEditingController emailPhoneNumber = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Please enter your email or phone number',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20.0, color: Colors.green),
              ),
              AppCard(
                child: TextFormField(
                  decoration:
                      InputDecoration(labelText: 'Email or Phone number'),
                  controller: emailPhoneNumber,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 7.0),
                width: 110,
                child: MaterialButton(
                  color: Colors.green[700],
                  textColor: Colors.white,
                  onPressed: () {
                    return showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          content: Text(
                            'A message has been sent to' +
                                '   ' +
                                emailPhoneNumber.text,
                            textAlign: TextAlign.center,
                          ),
                        );
                      },
                    );
                  },
                  child: Text(
                    'Submit',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
