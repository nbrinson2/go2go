import 'package:flutter/material.dart';
import '../common/app_card.dart';

class NumberConfirmation extends StatefulWidget {
  final String userName;
  final String email;
  final String phoneNumber;

  NumberConfirmation({Key key, this.userName, this.email, this.phoneNumber})
      : super(key: key);

  @override
  _NumberConfirmationState createState() => _NumberConfirmationState();
}

class _NumberConfirmationState extends State<NumberConfirmation> {
  @override
  Widget build(BuildContext context) {
    TextEditingController verificationNumber = new TextEditingController();
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 5.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(10),
            ),
            Text(
              'Thank you for registering ${widget.userName}',
              style: TextStyle(fontSize: 25.0, color: Colors.black),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
            ),
            Text(
              'A verification email has been sent to',
              style: TextStyle(fontSize: 20.0, color: Colors.black),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
            ),
            Text(
              '${widget.email}',
              style: TextStyle(fontSize: 20.0, color: Colors.red),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Text(
              'Also a verification code has been sent to',
              style: TextStyle(fontSize: 20.0, color: Colors.black),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
            ),
            Text(
              '${widget.phoneNumber}',
              style: TextStyle(fontSize: 30.0, color: Colors.red),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: AppCard(
                child: TextFormField(
                  decoration: InputDecoration(labelText: 'Verification Code'),
                  controller: verificationNumber,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 1.0),
              width: 135,
              child: MaterialButton(
                color: Colors.black,
                textColor: Colors.white,
                onPressed: () {},
                child: Text(
                  'Submit',
                  style: TextStyle(fontSize: 16),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 3.0),
              width: 175,
              child: MaterialButton(
                color: Colors.red,
                textColor: Colors.white,
                onPressed: () {},
                child: Text(
                  'Resend Verification',
                  style: TextStyle(fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
