import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../common/app_card.dart';
import '../screens/password-recovery.dart';
import '../screens/profile-page.dart';
import '../screens/userForm.dart';

class LoginPage extends StatefulWidget {
  final bool isLoggedIn;
  const LoginPage({Key key, this.isLoggedIn}) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isLoggedIn = false;

  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);

  _login() async {
    try {
      await _googleSignIn.signIn();
      setState(() {
        isLoggedIn = true;
      });
    } catch (error) {
      print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Center(
          child: Text(
            'GO2GO',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16.0, color: Colors.green[700]),
          ),
        ),
        AppCard(
          child: Container(
            margin: EdgeInsets.only(top: 2.0),
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(labelText: 'User Name'),
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Password'),
                ),
                Container(
                  margin: EdgeInsets.only(top: 6.0),
                  width: 160,
                  child: MaterialButton(
                    color: Colors.green[700],
                    textColor: Colors.white,
                    onPressed: () {
                      isLoggedIn = true;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Auth(isLoggedIn: isLoggedIn)),
                      );
                    },
                    child: Text(
                      'Login',
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PasswordRecovery()),
                      );
                    },
                    child: Text('Forgot Password ?'),
                  ),
                ),
              ],
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Create an account'),
            FlatButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => UserForm()),
                );
              },
              child: Text('Sign Up'),
            ),
          ],
        ),
        Container(
          margin: EdgeInsets.only(top: 1.0),
          width: 200,
          child: MaterialButton(
            color: Colors.blue,
            textColor: Colors.white,
            onPressed: () {
              _login();
            },
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(padding: EdgeInsets.only(right: 10.0)),
                Text(
                  'Sign-in with Facebook',
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 1.0),
          width: 212,
          child: MaterialButton(
            color: Colors.red,
            textColor: Colors.white,
            onPressed: () {
              _login();
            },
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.android, size: 30),
                Padding(padding: EdgeInsets.only(right: 10.0)),
                Text(
                  'Sign-in with Google',
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
          ),
        ),
      ],
    )));
  }
}
